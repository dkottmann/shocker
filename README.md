#Shocker#

Detects Shellshock vulnerabilities

Sample run:

    shocker urls.txt 10 —-concurrency 10 -v
    [+] Running with maximum 10 concurrent requests and 10 second delay
    [*] Testing http://10.0.1.31/
    [*] Testing http://10.0.1.31/cgi-bin
    [*] Testing http://10.0.1.31/cgi-bin/test.sh
    [*] Testing http://10.0.1.31/cgi-bin/test.pl
    [*] Testing http://10.0.1.31/index.html.orig
    [+] VULNERABLE: http://10.0.1.31/cgi-bin/test.pl
    [*] CLEAN: http://10.0.1.31/
    [*] CLEAN: http://10.0.1.31/index.html.orig
    [+] VULNERABLE: http://10.0.1.31/cgi-bin/test.sh
    [*] CLEAN: http://10.0.1.31/cgi-bin

    [+] The following 2 URL(s) may be vulnerable:
        [10s delay] - http://10.0.1.31/cgi-bin/test.pl
        [10s delay] - http://10.0.1.31/cgi-bin/test.sh

###Installation###
1. Install pip
2. Install Shocker with pip using the dist tarball

        sudo pip install Shocker-<version>.tar.gz
