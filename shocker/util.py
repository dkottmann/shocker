#!/usr/bin/env python
# Copyright (c) 2014 Dan Kottmann
# See the file license.txt for copying permission

def croak(data):
    """
    Display an error message and abort
    """
    print error("[!] Error: {0}".format(data))
    exit(1)


def warning(data):
    """
    format a string for 'yellow' colored terminal output
    """
    return '\033[93m' + data + '\033[0m'


def error(data):
    """
    format a string for 'red' colored terminal output
    """
    return '\033[91m' + data + '\033[0m'


def success(data):
    """
    format a string for 'green' colored terminal output
    """
    return '\033[92m' + data + '\033[0m'


def process_results(targets, vulnerable, delay, baseline=200, verbose=False):
    """
    processes a target list, pretty prints results, and builds list of success
    :param targets: List of Targets
    :param vulnerable: Dict of vulnerable hosts
    :param baseline: The baseline status code for failed auth
    :rtype : Boolean value indicating if any attempts were successful
    """
    for url, target in targets.items():
        if not target.resp_t or target.error:
            print error("[!] ERROR: {0}".format(target.url))
            continue

        target.roundtrip = (target.resp_t - target.req_t).seconds
        # Check if response status code was 200 (success)
        if target.roundtrip >= delay:
            print success("[+] VULNERABLE: {0}".format(target.url))

            # Add url to dict
            vulnerable[target.url] = target.roundtrip

        else:
            if verbose:
                print ("[*] CLEAN: {0}".format(target.url))

