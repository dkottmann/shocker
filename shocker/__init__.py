#!/usr/bin/env python
# Copyright (c) 2014 Dan Kottmann
# See the file license.txt for copying permission

class Target():

    def __init__(self, url, baseline=401):
        self.url = url
        self.status_code = baseline
        self.req_t = None
        self.resp_t = None
        self.roundtrip = 0
        self.vulnerable = False
        self.error = False

