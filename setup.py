from distutils.core import setup

setup(
    name="Shocker",
    version="0.2",
    author="Dan Kottmann",
    author_email="djkottmann@gmail.com",
    packages=["shocker"],
    scripts=["bin/shocker"],
    license="LICENSE.txt",
    url="http://bitbucket.org/dkottmann/shocker",
    description="Test for Shellshock vulnerabilities in web resources",
    install_requires=[
        "docopt >= 0.6.2",
        "gevent >= 1.0.1",
        "greenlet >= 0.4.4",
        "grequests >= 0.2.0",
        "requests >= 2.4.1",
        "wsgiref >= 0.1.2"
    ]
)
